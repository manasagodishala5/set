#The Python symmetric_difference() method returns the symmetric difference of two sets. The symmetric difference of two sets A and B is the set of elements that are in either A or B , but not in their intersection
s1 = {10,20,30,40,50,60}
s2 = {40,50,60,70,80,90}
s3 = s1.symmetric_difference(s2)
print(s3)

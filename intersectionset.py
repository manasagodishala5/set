#The intersection() method returns a set that contains the similarity between two or more sets.
s1 = {10,20,30,40,50,60}
s2 = {40,50,60,70,80,90}
s3 = s1.intersection(s2)
print(s3)

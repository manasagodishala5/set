#union
s1 = {10,20,30,40,50,60}
s2 = {40,50,60,70,80,90}
s3 = s1.union(s2)
print(s3)#The union() method returns a set that contains all items from the original set, and all items from the specified sets.
